# List
# Veriable name=[]

l1=[1,2,5,6,"test1","test2",1.2,True,False,2,5]

print(l1)
print(type(l1))

l1.append("added")
print(l1)
print(l1.pop())  #what is pop function remove last element from the list and return that element
print(l1)
l1.remove("test2")  # remove element from the list
print(l1)
print(l1.count("test1"))

# l1=[2,4,7,8,9,1,2,3,45]
# l1.sort()
# print(l1)

for i in l1:
    print(i)
    if i == 'test1':
        l1.append("after If")
        break



print(l1)

print(l1[3])

a=10
b=a
print(b)
print(a)
a=20
print(b)
print(a)


# Shallow copy and Deep copy difference
l2=l1
print(l2)
l1.append("app")
print("printing l1",l1)
print("printing l2",l2)

l2.append("app2")
print(l1)
print(l2)

print("Deep copy")

#Deep copy
l1=l2.copy()
print("printing l1 for deep copy",l1)
print("printing l1 for deep copy",l2)

l2.append("app3")

print(l1)
print(l2)

l3=l1+l2 #add element from 2 different list
print(l3)

l1="chandh"
l2="ddf"

print(l1)
print(l2)

