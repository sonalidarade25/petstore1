import requests
import json

base_url = "https://petstore.swagger.io/v2/pet/"
GET_PET_BY_TAGS = "findByTags?tags={}"
pet_tags = [41680794,-29245128,-55902183,67424482]

# Verify pet by tag
for tag in pet_tags:
     response = requests.get(url=base_url + GET_PET_BY_TAGS.format(tag))

     json_data=json.loads(response.text)
     # assert actual_data == expected_data ,"If you want to provide any msg"

     assert  response.status_code == 200

     print("****Veryfying test data for the tag {}".format(tag))
     for record in json_data:
         assert record[
                    'tags'][0]['id'] == tag, "status is not correct for id {} tag should be {} but showing {}".format(
             tag,record["id"], record["tags"][0]['id'])