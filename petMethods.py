# get,post,put,delete,patch

import requests
import json

base_url = "https://petstore.swagger.io/v2/pet/"

GET_PET_BY_STATUS = "findByStatus?status={}"
GET_PET_BY_TAGS = "findByTags?tags={}"

pet_status = ["available", "sold", "unavailable", "pending", "newstatus"]
pet_tags = ["string"]

# Verify pet by status
# for status in pet_status:
#     response=requests.get(url=base_url+GET_PET_BY_STATUS.format(status))
#
#     json_data=json.loads(response.text)
#     # assert actual_data == expected_data ,"If you want to provide any msg"
#
#     assert  response.status_code == 200
#
#     print("****Veryfying test data for the status {}".format(status))
#     for record in json_data:
#         assert record[
#                    'status'] == status, "status is not correct for id {} status should be available but showing {}".format(
#             record["id"], record["status"])

# Verify pet by tag
# for tag in pet_tags:
#     response = requests.get(url=base_url + GET_PET_BY_TAGS.format(tag))
#
#     json_data=json.loads(response.text)
#     # assert actual_data == expected_data ,"If you want to provide any msg"
#
#     assert  response.status_code == 200
#
#     print("****Veryfying test data for the tag {}".format(tag))
#     for record in json_data:
#         assert record[
#                    'tags'][0]['name'] == tag, "status is not correct for id {} tag should be {} but showing {}".format(
#             tag,record["id"], record["tags"][0]['name'])

# Create pet using post method

body = {
        "name": "{}",
        "photoUrls": [
            "dolore Excepteur est",
            "consequat in labore"
        ],
        "id": 41680794,
        "category": {
            "id": -29245128,
            "name": "incididunt"
        },
        "tags": [
            {
                "id": -55902183,
                "name": "Lorem laboris nulla"
            },
            {
                "id": 67424482,
                "name": "incididunt ipsum eiusmod"
            }
        ],
        "status": "{}"
    }

headers = {"Content-Type": "application/json"}

for status in pet_status:
    body['status'] = status
    response = requests.post(url=base_url, json=body, headers=headers)
    response.status_code = 200
    assert response.status_code == 200, "status is not as per expectation"
    json_data = json.loads(response.text)
    assert json_data['status'] == status

    print(response)

    # create pet
    # verify pet is created in get response using petid
    # verify pet is listed in gets pet by status api