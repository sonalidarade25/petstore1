import pytest
import requests
import json
import yaml
import os
from libs.api_body import ApiBody
import random

class PetLibs(ApiBody):

    def __init__(self):
        file_path="configs/{}.yaml".format(os.environ['TEST_ENV'])
        print("Executing test case on the {} environment".format(os.environ['TEST_ENV']))
        with open(file_path, 'r') as f:
            self.configs = yaml.load(f)

        self.base_url=self.configs.get('base_url')

    def get_pet(self , url):
        print("from pet libs of get pet method")
        response = requests.get(url = self.base_url + url)
        assert response.status_code == 200
        json_data = json.loads(response.text)
        print("response converted to json")
        return json_data

    def get_random_id(self):
        id = random.randrange(1, 1000 ** 3)
        return id

    def create_pet(self ,name='test_name',status='sold'):
        body = self.create_pet_body
        pet_id =self.get_random_id()
        body['id']=pet_id
        body['status']=status
        body['name']=name
        headers = {"Content-Type": "application/json"}
        response = requests.post(url=self.base_url, json=body, headers=headers)
        assert response.status_code == 200
        print("pet is created with id: {} and name:{} with status:{}".format(id,name,status))
        json_data = json.loads(response.text)

        return json_data

    def get_pet_id(self ,id):
        print("from pet libs of get pet method")
        response = requests.get(url = self.base_url + str(id))
        assert response.status_code == 200
        json_data = json.loads(response.text)
        print("response converted to json")
        return json_data