import requests
import json

def get_token():
    auth_data={"client_id":"api-client","client_secret":"942d36a36d6bf422a36f5871f905b6e5","grant_type":"client_credentials"}
    response=requests.post("https://api-sandbox.orangehrm.com/oauth/issueToken",json=auth_data)
    json_data=json.loads(response.text)
    token=json_data['access_token']
    return token

token=get_token()

headers={"Authorization":"Bearer {}".format(token)}
response=requests.get(url="https://api-sandbox.orangehrm.com/api/employmentStatus?limit=10&sortingFeild=id&sortingOrder=asc",headers=headers)
print(response.text)


