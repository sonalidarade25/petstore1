import pytest


# class name should start with Test

@pytest.mark.pettests
class TestPet:
    @pytest.fixture

    @pytest.mark.parametrize('status', ['available', 'sold', 'unsold', 'unavailable'])
    @pytest.mark.pet
    def test_get_pet(self, status, libs):
        libs.get_pet(self, url=self.base_url)
        print("parameter is {}".format(status))
        print("i am in get pet")


    # test name should with test
    @pytest.mark.pet
    def test_post_pet(self):
        print("i am in post pet")

    @pytest.mark.pet
    def test_put_pet(self):
        print("i am in put pet")

    @pytest.mark.pet
    def test_delete_pet(self):
        # assert False,"test case is not pass"
        print("i am in delete pet")

    @pytest.mark.store
    def test_post_store(self):
        print("i am in post pet")

    @pytest.mark.store
    def test_put_store(self):
        print("i am in put pet")

    @pytest.mark.store
    def test_delete_store(self):
        # assert False,"test case is not pass"
        print("i am in delete pet")
