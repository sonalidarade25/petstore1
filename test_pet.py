import pytest as pytest
import libs
from libs.pet_libs import PetLibs

class TestPet():
    #base_url = "https://petstore.swagger.io/v2/pet/"


   #def __init__(self):
   #print("i am in constructor")
                     #base_url = "https://petstore.swagger.io/v2/pet/"

    @pytest.mark.parametrize('status', ['available', 'sold', 'unsold', 'unavailable'])
    def test_get_pet_by_status(self,status,libs,apis):
                        print("from tests package verify status by get_status")
                        reps=libs.get_pet(apis.GET_PET_BY_STATUS.format(status))
                        print(reps)
                        for record in reps:
                            assert record['status'] == status, "status is not correct for id {} status should be available but showing {}".format(
                                        record["id"], record["status"])

    @pytest.mark.parametrize('tag_name', ['string'])
    def test_get_pet_by_tag(self, tag_name, libs,apis):
                        print("****Veryfying test data for the tag {}".format(tag_name))
                        reps = libs.get_pet(url=apis.GET_PET_BY_TAGS)
                        print(reps)
                        for record in reps:
                                 assert record['tags'][0]['name'] == tag_name, "status is not correct for id {} tag " \
                                                                               "should be {} but showing {}".format(
                                     tag_name,record["id"], record["tags"][0]['name'])

    @pytest.mark.pet
    def test_create_pet(self,libs):
                        status="sold"
                        resp=libs.create_pet(name="excuting from create pet",status=status)
                        assert resp['status']==status


    @pytest.mark.parametrize('id', ['string'])
    def test_verify_end_to_end_scenario(self, tag_name, libs, apis):
                        status='available'
                        resp=libs.create_pet(name="excuting from create pet",status=status)
                        pet_id =resp['id']
                        reps = libs.get_pet_by_id(pet_id)

                        print(reps)
                        assert reps['status'] == status, "status is not correct for id {} and status :{}".\
                                format(reps['id'],reps['status'])
                        assert reps['id']==pet_id

                #ob=TestPet()
                #ob.test_get_pet_by_status('available')