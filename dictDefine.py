# Dictionry
#username,password,address
l1=["ss",1212,"add"]
dict2={121}
print(type(dict2))
dict1={"username":"ss","password":1212,"address":"add"}
print(dict1)
print(dict1.get("username"))
print(dict1.pop("password"))
print(dict1)
#print(dict1.popitem())

dict1["pass"]="this is pass"
print(dict1)

dict1.update({"address":"address1212"})
print(dict1)
dict1["address"]="updated"
print(dict1)

print(type(dict1.keys()))

for i in dict1.keys():
    if i=="pass":
        print(dict1[i])
        #assert dict1[i]==1212,"password not matching"

print(dict1)
for i,j in dict1.items():
    print("this is key",end=" ")
    print(i)
    print("this is value",end=" ")
    print(j)


dict3={"address":"address1212","names":["a","sonali","darade","asha"],"marks":{"math":12,"sci":45,"bio":99,"listmarks":[12,45,99],"internal":{"math":2323,"dfdf":2323}}}

for i in dict3['names']:
    print(i)

for i,j in (dict3["marks"]).items():
    print(f"marks {i}={j}")
print(dict3['names'])
print(dict3['marks']['listmarks'])
print(dict3['marks']['internal']['math'])